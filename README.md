## Orckid Lab scaffold of Laravel framework

### Installation
Run the following command to create a new project:

    composer create-project orckid-lab/laravel project-name --prefer-dist
    
Install node dependencies and compile assets
    
    npm install && npm run dev

### Modules installed
- Roles and permission (https://github.com/spatie/laravel-permission)
- Tags (https://github.com/spatie/laravel-tags)
- Telescope
- Horizon

### Handling tags

####Delete local tags.
git tag -d $(git tag -l)
####Fetch remote tags.
git fetch
####Delete remote tags.
git push origin --delete $(git tag -l) # Pushing once should be faster than multiple times
####Delete local tags.
git tag -d $(git tag -l)



### Sync with repo
Clone the repository

    git clone git@bitbucket.org:orckidlab/laravel.git
    
Add the repository path to remote list

    git remote add upstream git@bitbucket.org:orckidlab/laravel.git
    
Fetch changes from the repository

    git fetch upstream --no-tags
    
Go back to the branch to merge with
    
    git checkout master
    
Merge with upstream

    git merge upstream/master


### Add tag
- git tag tagname

- git push origin --tags
