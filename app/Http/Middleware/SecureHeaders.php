<?php

namespace App\Http\Middleware;

use Closure;

class SecureHeaders
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->headers->set('Server', env('APP_NAME'));
        $response->headers->set('Cache-Control', 'no-cache, must-revalidate');
        $response->headers->set('Expect-CT', 'enforce, max-age=30');
        $response->headers->set('Referrer-Policy', 'no-referrer-when-downgrade');
        $featurePolicies = [
            'sync-xhr' => 'self',
            'geolocation' => 'self',
            'camera' => 'none',
            'speaker' => 'self',
            'fullscreen' => 'self',
            'payment' => 'self',
            'microphone' => 'self',
            'midi' => 'none',
            'magnetometer' => 'none',
            'gyroscope' => 'none',
        ];
        $formattedFeatures = collect($featurePolicies)->map(function ($value, $key) {
                return "$key '$value'";
            })->implode(';') . ';';
        $response->headers->set('Feature-Policy', $formattedFeatures);
        return $response;
    }
}
