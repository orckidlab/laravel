<?php

namespace App\Orckid;

class AppJavascript
{
    protected $javascript = [];

    public function build()
    {
        $this->add('routes', [

        ]);

        $this->add('GA_KEY', env('GA_KEY'));
    }

    public function add($key, $value)
    {
        $this->javascript[$key] = $value;

        return $this;
    }

    public function render()
    {
        $this->build();

        $output = json_encode($this->javascript);

        return "<script>window.Laravel = $output</script>";
    }
}
