<?php

namespace App\Orckid;

use App\User;

/**
 * Class Auth
 * @package App\OluwaJealous
 */
class Auth
{
    /**
     * @var User
     */
    public $user;

    /**
     * Auth constructor.
     */
    public function __construct()
    {
        $this->user = \Illuminate\Support\Facades\Auth::user();
    }
}
