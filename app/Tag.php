<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Spatie\Tags\HasTags;

/**
 * Class Tag
 * @package App
 */
class Tag extends \Spatie\Tags\Tag
{
    public static function findFromString(string $name, string $type = null, string $locale = null)
    {
        $locale = $locale ?? app()->getLocale();

        return static::query()
            ->whereRaw('JSON_EXTRACT(name, "$.?") = "?"', [
                $locale,
                $name,
            ])
            ->where('type', $type)
            ->first();
    }
}
