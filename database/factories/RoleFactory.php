<?php

use Faker\Generator as Faker;

$factory->define(App\Role::class, function (Faker $faker) {
    $name = join(' ', $faker->words);
    return [
        'name' => str_slug($name),
        'label' => $name,
    ];
});
