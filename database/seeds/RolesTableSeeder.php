<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->warn('Roles seeder in progress...');

        $roles = [
            'super-admin' => 'Super administrator',
            'administrator' => 'Administrator',
        ];

        foreach ($roles as $name => $label) {
            \App\Role::create([
                'name' => $name,
                'label' => $label,
            ]);

            $this->command->info("Role $name created.");
        }

        $this->command->info('Roles seeder completed');
    }
}
