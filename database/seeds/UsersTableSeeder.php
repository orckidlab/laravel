<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);

        $this->command->warn('Users seeder in progress...');

        $orckid = [
            [
                'first_name' => 'Joey',
                'last_name' => '',
                'email' => 'joey@orckid.com',
            ],
            [
                'first_name' => 'Ash',
                'last_name' => 'Joory',
                'email' => 'ash@orckid.com',
            ],
            [
                'first_name' => 'Ashley',
                'last_name' => 'Lutchumun',
                'email' => 'ashley@orckid.com',
            ],
            [
                'first_name' => 'Daniel',
                'last_name' => 'Allen',
                'email' => 'd.allen@orckid.com',
            ],
        ];

        foreach ($orckid as $data) {
            $password = bcrypt(env('SEEDER_PASSWORD', 'password'));

            $user = new User($data);

            $user->password = $password;

            $user->save();

            $this->command->info("User $user->email created.");

            $user->assignRole('super-admin');

            $this->command->info("$user->email assigned role super-administrator.");
        }

        $this->command->info('Users seeder completed');
    }
}
