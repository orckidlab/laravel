import Vue from 'vue'
import Buefy from 'buefy'
import VueHelpers from '@wyxos/vue-helpers'
import Navigation from '@js/plugins/navigation'
import Header from '@js/plugins/header'

window.$ = window.jQuery = require('jquery')

Vue.use(VueHelpers)
Vue.use(Buefy)
Vue.use(Navigation)
Vue.use(Header)

const app = new Vue({
  data: {}
})

app.$mount('#app')
