@if (app()->environment() === 'production' && env('GA_KEY'))
    <script async
            src="https://www.googletagmanager.com/gtag/js?id={{ env('GA_KEY') }}"></script>
@endif
