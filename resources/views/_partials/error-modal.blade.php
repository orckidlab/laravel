<div class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Error</p>
            <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body">
            <article class="content">
                <p v-if="$errors.isStatus(503)">The site is currently in maintenance. Try again in a moment.</p>
                <p v-if="$errors.isStatus(403)">This action is not allowed.</p>
                <p v-if="$errors.isStatus(500)">An unexpected error occurred.</p>
                <p v-if="$errors.isStatus(419)">The form has expired. Please reload the page and try again.</p>
                <p v-if="$errors.isStatus(422)">There is an error within the form or fields required for this action. <br>Please
                    review.</p>
            </article>
        </section>
        <footer class="modal-card-foot">
            <button class="button is-success">Close</button>
            <button class="button">Cancel</button>
        </footer>
    </div>
</div>
