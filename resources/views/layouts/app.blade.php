@extends('layouts.core')

@section('template')
    @include('_partials.header')
    <main id="main-content">
        @yield('content')
    </main>
    @include('_partials.footer')
@endsection
