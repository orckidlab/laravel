<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>@yield('title', 'Default title') - {{ config('app.name') }}</title>
    <meta name="description" content="@yield('description', 'Default description')">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, viewport-fit=cover">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="author" content="{{ config('app.name') }}"/>
    @if(env('SOCIAL_TAGS'))
        <meta name="twitter:title" content="@yield('title', 'Default title')"/>
        <meta name="twitter:description" content="@yield('description', 'Default description')"/>
        <meta name="twitter:image" content="/"/>
        <meta property="og:title" content="@yield('title', 'Default') - {{ config('app.name') }}"/>
        <meta property="og:description" content="@yield('description', 'Default description')"/>
        <meta property="og:locale" content="{{ str_replace('_', '-', app()->getLocale()) }}"/>
        <meta property="og:url" content="{{ url()->current() }}"/>
        <meta property="og:image" content="/"/>
    @endif
    <link rel="canonical" href="{{ url()->current() }}"/>
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com"/>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}"/>
    @stack('stylesheets')
    @php
        $javascript->add('user', $auth);
    @endphp
    {!! $javascript->render() !!}
    <script defer src="{{ mix('js/manifest.js') }}"></script>
    <script defer src="{{ mix('js/vendor.js') }}"></script>
    <script defer src="{{ mix('js/app.js') }}"></script>
    @stack('scripts')
    @include('_partials.analytics')
</head>
<body>
<div id="app">
    <div id="global">
        @yield('template')
        @include('_partials.form-logout')
        @include('_partials.error-modal')
    </div>
</div>
</body>
</html>
