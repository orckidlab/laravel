<?php

namespace Tests\Feature\Page;

use App\User;
use Tests\TestCase;

/**
 * Class HomeTest
 * @package Tests\Feature\Page
 */
class HomeTest extends TestCase
{
    /** @test */
    public function guest_can_view()
    {
        $this->get(route('home'))
            ->assertSuccessful();
    }

    /** @test */
    public function authenticated_user_can_view()
    {
        $this->actingAs(create(User::class));

        $this->get(route('home'))
            ->assertSuccessful();
    }
}
