<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;

class FactoryTest extends TestCase
{
    /** @test */
    public function user()
    {
        $this->assertInstanceOf(User::class, create(User::class));
    }
}
