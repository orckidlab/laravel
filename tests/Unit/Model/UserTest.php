<?php

namespace Tests\Unit\Model;

use App\Role;
use App\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function belongs_to_many_roles()
    {
        /** @var User $user */
        $user = create(User::class);

        $roleA = create(Role::class, [
            'name' => 'super-administrator',
        ]);

        $roleB = create(Role::class, [
            'name' => 'super-administrator',
        ]);

        $user->assignRole([
            $roleA->name,
            $roleB->name,
        ]);

        $this->assertTrue($user->hasAllRoles(collect([
            $roleA,
            $roleB,
        ])));
    }
}
