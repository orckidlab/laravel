<?php

namespace Tests\Unit\Seeder;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class RolesTableTest extends TestCase
{
    /** @test */
    public function seeder()
    {
        Artisan::call('db:seed', [
            '--class' => 'RolesTableSeeder',
        ]);

        $this->assertDatabaseHas('roles', [
            'label' => 'Administrator',
            'name' => 'administrator',
        ]);
    }
}
