<?php

namespace Tests\Unit\Seeder;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class TagsTableTest extends TestCase
{
    /** @test */
    public function seeder()
    {
        Artisan::call('db:seed', [
            '--class' => 'TagsTableSeeder',
        ]);

        $this->assertTrue(true);
    }
}
