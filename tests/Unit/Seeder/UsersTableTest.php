<?php

namespace Tests\Unit\Seeder;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class UsersTableTest extends TestCase
{
    /** @test */
    public function seeder()
    {
        Artisan::call('db:seed', [
            '--class' => 'UsersTableSeeder',
        ]);

        $this->assertDatabaseHas('users', ['email' => 'joey@orckid.com']);
    }
}
