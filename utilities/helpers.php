<?php

/**
 * @param $class
 * @param null $count
 * @param array $attributes
 * @return mixed
 */
function create($class, $count = null, $attributes = [])
{
    return customFactory($class, $count, $attributes, 'create');
}

/**
 * @param $class
 * @param null $count
 * @param array $attributes
 * @return mixed
 */
function make($class, $count = null, $attributes = [])
{
    return customFactory($class, $count, $attributes, 'make');
}

/**
 * @param $class
 * @param null $count
 * @param array $attributes
 * @return mixed
 */
function raw($class, $count = null, $attributes = [])
{
    return customFactory($class, $count, $attributes, 'raw');
}

/**
 * @param $class
 * @param $count
 * @param $attributes
 * @param $method
 * @return mixed
 */
function customFactory($class, $count, $attributes, $method)
{
    if (is_array($count)) {
        $attributes = $count;
        $count = null;
    }

    return factory($class, $count)->{$method}($attributes);
}

/**
 * @var \Illuminate\Database\Eloquent\Model $class
 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
 */
function random($class)
{
    return $class::query()->inRandomOrder()->first();
}

/**
 * @param \Illuminate\Database\Eloquent\Builder $query
 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
 */
function randomFromQuery(\Illuminate\Database\Eloquent\Builder $query)
{
    return $query->inRandomOrder()->first();
}
