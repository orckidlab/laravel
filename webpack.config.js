const path = require('path')

let mixConfig = {
  module: {
    rules: []
  },
  plugins: [],
  devServer: {
    overlay: true
  },
  resolve: {
    alias: {
      '@js': path.resolve(__dirname, 'resources/js'),
      '@svg': path.resolve(__dirname, 'resources/svg'),
      '@plugins': path.resolve(__dirname, 'resources/js/plugins'),
      '@components': path.resolve(__dirname, 'resources/js/components'),
      '@tests': path.resolve(__dirname, 'tests/Javascript')
    }
  },
  watchOptions: {
    ignored: /node_modules/
  }
}

module.exports = mixConfig
