const mix = require('laravel-mix')
let StylelintPlugin = require('stylelint-webpack-plugin')

let webpackConfig = require('./webpack.config')

// @See https://laravel-mix.com/docs/5.0/options
mix.options({
  extractVueStyles: true,
  processCssUrls: false,
  terser: {},
  purifyCss: false,
  clearConsole: true,
  postCss: [require('autoprefixer')],
  cssNano: {
    discardComments: {
      removeAll: true
    },
    uniqueSelectors: true,
    discardDuplicates: true
  }
})

// Compile & Extract
mix
  .setPublicPath('public')
  .js('resources/js/app.js', 'public/js')
  .sass('resources/sass/app.scss', 'public/css/')
  .extract([
    'vue',
    'buefy',
    'jquery',
    'axios',
    'lodash'
  ])

// Hash & version files in production
if (mix.inProduction()) {
  mix.version()
}

// Source maps when not in production
if (!mix.inProduction()) {
  webpackConfig.module.rules.push({
    test: /\.(js|vue)$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'eslint-loader',
    enforce: 'pre',
    options: {
      formatter: require('eslint-friendly-formatter'),
      fix: true
    }
  })

  webpackConfig.devServer = { overlay: true }

  mix.browserSync({
    https: {
      key: './../../certificates/localhost.key',
      cert: './../../certificates/localhost.crt'
    },
    browser: 'Chrome',
    proxy: process.env.APP_URL
  })

  webpackConfig.plugins.push(new StylelintPlugin({
    files: './resources/sass/**/*.scss',
    configFile: './.stylelintrc.json',
    fix: true
  }))

  mix.sourceMaps()
}

mix.webpackConfig(webpackConfig)
mix.disableNotifications()
